<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Data extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Data_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('data/data_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Data_model->json();
    }

    public function read($id) 
    {
        $row = $this->Data_model->get_by_id($id);
        if ($row) {
            $data = array(
		'no' => $row->no,
		'ID' => $row->ID,
		'nama' => $row->nama,
		'asal' => $row->asal,
		'gabung' => $row->gabung,
	    );
            $this->load->view('data/data_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('data'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('data/create_action'),
	    'no' => set_value('no'),
	    'ID' => set_value('ID'),
	    'nama' => set_value('nama'),
	    'asal' => set_value('asal'),
	    'gabung' => set_value('gabung'),
	);
        $this->load->view('data/data_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'ID' => $this->input->post('ID',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'asal' => $this->input->post('asal',TRUE),
		'gabung' => $this->input->post('gabung',TRUE),
	    );

            $this->Data_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('data'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Data_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('data/update_action'),
		'no' => set_value('no', $row->no),
		'ID' => set_value('ID', $row->ID),
		'nama' => set_value('nama', $row->nama),
		'asal' => set_value('asal', $row->asal),
		'gabung' => set_value('gabung', $row->gabung),
	    );
            $this->load->view('data/data_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('data'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('no', TRUE));
        } else {
            $data = array(
		'ID' => $this->input->post('ID',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'asal' => $this->input->post('asal',TRUE),
		'gabung' => $this->input->post('gabung',TRUE),
	    );

            $this->Data_model->update($this->input->post('no', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('data'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Data_model->get_by_id($id);

        if ($row) {
            $this->Data_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('data'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('data'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('ID', 'id', 'trim|required');
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('asal', 'asal', 'trim|required');
	$this->form_validation->set_rules('gabung', 'gabung', 'trim|required');

	$this->form_validation->set_rules('no', 'no', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
    
    public function lihatandroid(){
        //meload helper    
        $this->load->helper('json');
        //menerima kirimin dari URL atau dari Android
        $xSearch = $_POST['search'];

        //mendeklarasikan json_data dengan format json_data['nama kolom ditable]
        $this->json_data['no'] = "";
        $this->json_data['ID'] = "";
        $this->json_data['nama'] = "";
        $this->json_data['asal'] = "";
        $this->json_data['gabung'] = "";
        
        //meload datamodel
        $this->load->model('Data_model');
        //deklarasi array
        $response = array();
        
        //memasukan hasil query get all yang ada di class model dengan nama function ambil()
        $xQuery = $this->Data_model->ambil();
        //dilooping sejumlah hasil lalu dianggqap sebagai row
        foreach ($xQuery->result() as $row) {
           
            //mendeklarasikan json_data dengan format json_data['nama kolom ditable] dengan isi
            //row adalah penganggapan kolom di hasil querry
            //no, id, nama, asal, dan gabung adalah nama kolom
            $this->json_data['no'] = $row->no;
            $this->json_data['ID'] = $row->ID;
            $this->json_data['nama'] = $row->nama;
            $this->json_data['asal'] = $row->asal;
            $this->json_data['gabung'] = $row->gabung;

            //memasukan json kedalam array variabel response
            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        //mencetak variabel array respon yang berisi json 
        echo json_encode($response);
    }

}
