<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index() {
        $this->load->view('user/user_list');
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->User_model->json();
    }

    public function read($id) {
        $row = $this->User_model->get_by_id($id);
        if ($row) {
            $data = array(
                'idx' => $row->idx,
                'nama' => $row->nama,
                'alamat' => $row->alamat,
                'user' => $row->user,
                'password' => $row->password,
            );
            $this->load->view('user/user_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('user/create_action'),
            'idx' => set_value('idx'),
            'nama' => set_value('nama'),
            'alamat' => set_value('alamat'),
            'user' => set_value('user'),
            'password' => set_value('password'),
        );
        $this->load->view('user/user_form', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
                'alamat' => $this->input->post('alamat', TRUE),
                'user' => $this->input->post('user', TRUE),
                'password' => $this->input->post('password', TRUE),
            );

            $this->User_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('user'));
        }
    }

    public function update($id) {
        $row = $this->User_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('user/update_action'),
                'idx' => set_value('idx', $row->idx),
                'nama' => set_value('nama', $row->nama),
                'alamat' => set_value('alamat', $row->alamat),
                'user' => set_value('user', $row->user),
                'password' => set_value('password', $row->password),
            );
            $this->load->view('user/user_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('idx', TRUE));
        } else {
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
                'alamat' => $this->input->post('alamat', TRUE),
                'user' => $this->input->post('user', TRUE),
                'password' => $this->input->post('password', TRUE),
            );

            $this->User_model->update($this->input->post('idx', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('user'));
        }
    }

    public function delete($id) {
        $row = $this->User_model->get_by_id($id);

        if ($row) {
            $this->User_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('user'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_rules('user', 'user', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');

        $this->form_validation->set_rules('idx', 'idx', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    //=========READ=========
    public function readuserAndroid() {
        $this->load->helper('json');

        $xSearch = $_POST['search'];

        $this->json_data['idx'] = "";
        $this->json_data['nama'] = "";
        $this->json_data['alamat'] = "";
        $this->json_data['user'] = "";
        $this->json_data['password'] = "";

        $this->load->model('User_model');

        $response = array();

        $xQuery = $this->User_model->getListuser();


        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['nama'] = $row->nama;
            $this->json_data['alamat'] = $row->alamat;
            $this->json_data['user'] = $row->user;
            $this->json_data['password'] = $row->password;
            array_push($response, $this->json_data);
        }


        if (empty($response)) {

            array_push($response, $this->json_data);
        }


        echo json_encode($response);
    }

    //=========READ=========
    //=========INSERT AND UPDATE=========
    public function simpanupdateuserAndroid() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {

            $xidx = $_POST['edidx'];
        } else {

            $xidx = '0';
        }

        $xnama = $_POST['ednama'];
        $xalamat = $_POST['edalamat'];
        $xuser = $_POST['eduser'];
        $xpassword = $_POST['edpassword'];

        $response = array();
        $this->load->model('User_model');

        if ($xidx != '0') {
            //===UPDATE===

            $xStr = $this->User_model->Updateuser($xidx, $xnama, $xalamat, $xuser, $xpassword);
        } else {
            //===INSERT===

            $xStr = $this->User_model->Insertuser($xidx, $xnama, $xalamat, $xuser, $xpassword);
        }

        $row = $this->User_model->getLastIndeximagedetail();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['nama'] = $row->nama;
        $this->json_data['alamat'] = $row->alamat;
        $this->json_data['user'] = $row->user;
        $this->json_data['password'] = $row->password;
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

//=========INSERT AND UPDATE=========
}
